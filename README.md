# lookme-context-support

## 紹介

コンポーネントでアプリケーションを開発しやすくために作ったライブラリです。

## 機能と特徴

* 一箇所でコンポーネントを定義する
* 自動セットアップ
* プロファイル
* リロード

## 使い方

### Quick Start

    require 'lookme-context-support'

    class Foo
      def run
        puts 'run'
      end
    end

    LookmeContextSupport::ApplicationContext.configure do |container|
      container.register(:foo, Foo.new)
    end

    foo = LookmeContextSupport::ApplicationContext.get_default_context.get_object(:foo)
    foo.run

ここでは、Fooのインスタンスをlookme-context-supportに管理され、あとでAPIで取れます。

１つのインスタンスなら特に意味ないですが、依存してるコンポーネントを見ればそのメリットが分かるはずです。

    class Foo
      def run
        puts 'run'
      end
    end

    class Bar
      attr_writer :foo

      def initialize
        @foo = nil
      end

      def run
        puts 'before run'
        @foo.run
      end
    end

    LookmeContextSupport::ApplicationContext.configure do |container|
      container.register_component(:foo, Foo)
      container.register_component(:bar, Bar)
    end

    bar = LookmeContextSupport::ApplicationContext.get_default_context.get_object(:bar)
    bar.run

ここでは、Barが依存しているFooのインスタンスがlookme-context-supportで自動的にセットアップしてくれます。
いつ、どこでセットされたのはBarにとって気にすることではありません。

### API

lookme-context-supportにインスタンスを登録するには、３つのAPIあります。

* container.register(key, instance) 直接初期化
* container.register(key, &block) 複雑な初期化
* container.register_component(key, class, options = {}) lookme-context-supportに初期化してもらう

lookme-context-supportに初期化してもらうと、依頼しているインスタンスは先に登録しないとエラーになります。
セットする基準は、 `=xxx` メソッドがあることです。attr_writerでしたら、自動的にそのメソッドを作成します。

オプション

* mapping: Hash<Symbol, Symbol> 依存しているインスタンスと属性名が一致しない場合使う
* optional: true|false 依存しているインスタンスがなくてもエラーにならない
* ignored: Array<Symbol> セットしたくない属性

サンプルコード

    class Foo
      def run
        puts 'run'
      end
    end

    class Bar
      attr_writer :foo1
      attr_writer :foo2
      attr_writer :foo3

      def initialize
        @foo1 = nil
        @foo2 = nil
        @foo3 = nil
      end

      def run
        puts 'before run'
        @foo1.run
      end
    end

    LookmeContextSupport::ApplicationContext.configure do |container|
      container.register_component(:foo, Foo)
      container.register_component(:bar, Bar, mapping: {foo1: :foo}, optional: true, ignored: [:foo3])
    end


このコードでは、foo1はfooインスタンスになり、foo3はセットされず、foo2はスルーされました。エラーになりません。

* container.get(key)

インスタンスを初期化中、別のインスタンスを依存してますが、そのまま自動セットアップ機能を使えなく、手動で登録したインスタンスを取得する場合使います。
例えば、設定が必要な初期化

    LookmeContextSupport::ApplicationContext.configure do |container|
      container.register(:configuration) do
        # do stuff
      end

      container.register(:xxx_http_client) do
        app_config = container.get(:configuration)

        client_config = LookmeHttpApiClient::HttpClientConfig.new
        client_config.host = app_config['api_server']['http_host'].not_blank
        client_config.port = app_config['api_server']['http_port'].as_int
        client_config.base_path = '/'
        client_config.timeout = app_config['api_server']['timeout'].as_int

        LookmeHttpApiClient::HttpClient.build(client_config)
      end
    end

### インスタンス取得

これは主にコンテナーの外からインスタンスを取得したい場合です。

直接取ってくる

LookmeContextSupport::ApplicationContext.get_default_context.get_object(:component_name)

ApplicationContextSupport

    class FooController
      include LookmeContextSupport::ApplicationContextSupport

      def action1
        get_object_by_name(:component_name)
      end
    end

ApplicationContextHelper

クラスメソッドでしか取得できないクラスで使う想定

    class FooApi < Grape::API
      helpers LookmeContextSupport::ApplicationContextHelper
      helpers do
        def foo_service
          get_object_by_name(:foo_service)
        end
      end

      get '/' do
        foo_service.count
      end
    end

### プロファイル

場合によって別々のコンテナーが必要になるかもしれませんので、機能では複数おプロファイルを作れるようになっています。
いまデフォルトではdefaultコンテナーになります。

    LookmeContextSupport::ApplicationContext.configure(profile: :another_profile) do |container|
    end

    LookmeContextSupport::ApplicationContext.get_context(:another_profile).get_object(:component_name)

    class FooController
      include LookmeContextSupport::ApplicationContextSupport

      use_application_context_profile_name :another_profile

      def action1
        get_object_by_name(:component_name)
      end
    end

ApplicationContextHelperにprofileの指定はできません。

### リロード

Rails開発環境のautoload機能で、autoload pathに入ってるクラスのインスタンスがどこかに持ってると、クラス自体がリロードされたら、エラーになる可能性があります。
１つの解決策は、クラスのリロードと共にコンテナーもリロードすることです。

    LookmeContextSupport::ApplicationContext.configure(reloadable: true) do |container|
    end

    unless Rails.configuration.cache_classes
      ActionDispatch::Reloader.to_prepare do
        LookmeContextSupport::ApplicationContext.reload_default_context!
      end
    end

defaultではないプロファイルは、LookmeContextSupport::ApplicationContext.reload_context!(profile_name)を使えます。

## コンポーネントで開発する

コンポーネントで開発すると、機能を細かく分解し、汎用性の高いクラスを作成し、コンポーネントの組み合わせで機能を提供できます。
そしてコンポーネントの依頼関係はlookme-context-supportに管理され、コンポーネント自体は依存しているクラスのインスタンスがどういう形で初期化されるのか知らないままで開発できます。
メリットとしては、クラス・モジュールの間で結合度は低くなり、機能をフォーカスして実装できます。