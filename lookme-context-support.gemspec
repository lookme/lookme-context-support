# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'lookme_context_support/version'

Gem::Specification.new do |s|
  s.name = 'lookme-context-support'
  s.version = LookmeContextSupport::VERSION
  s.authors = ['chen.zhao']
  s.summary = 'lookme context support'

  s.add_runtime_dependency 'lookme-logging', '~> 0.0.1'

  s.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  s.require_paths = ['lib']
end
