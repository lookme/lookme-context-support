module LookmeContextSupport
  class ObjectRepository
    LOG_TAG = self.name

    # @param [Hash] mapping
    def initialize(mapping = {})
      @logger = LookmeLogging::LoggerManager.find_logger(self.class)
      @mapping = mapping
    end

    # @param [Symbol] name
    # @param [Object] object
    def register_object(name, object)
      if @mapping.include?(name)
        raise ObjectExistsError, "object #{name} exists"
      end
      @logger.info(LOG_TAG) {"register object #{name}, class #{object.class}"}
      @mapping[name] = object
    end

    # @param [Symbol] name
    # @param [Boolean] required
    def get_object(name, required)
      object = @mapping[name]
      if required && object.nil?
        raise ObjectNotFoundError, "object #{name} not found"
      end
      object
    end
  end
end