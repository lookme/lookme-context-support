module LookmeContextSupport
  class ApplicationContextBuilder
    LOG_TAG = self.name

    NOT_ATTR_WRITE_METHODS = ::Set.new([:===, :==, :!=, :>=, :<=, :!=])

    def initialize
      @logger = LookmeLogging::LoggerManager.find_logger(self.class)
      @logger_assembler = LookmeLogging::LoggerManager.find_logger(ComponentAssembler)
      @object_repository = ObjectRepository.new
    end

    # @param [Symbol] name
    # @param [Object] object
    def register(name, object = nil)
      @object_repository.register_object(name, object || yield)
    end

    # @param [Symbol] name
    # @param [Class] klass
    # @param [Hash] options
    def register_component(name, klass, options = {})
      assembler = ComponentAssembler.new(@object_repository, name, klass, @logger_assembler)
      assembler.dependency_mapping = options[:mapping]
      assembler.dependency_required = !options[:optional]
      assembler.attribute_ignored = options[:ignored]
      @object_repository.register_object(name, assembler.assemble)
    end

    # @param [Symbol] name
    # @return [Object]
    def get(name)
      @object_repository.get_object(name, true)
    end

    # @return [LookmeContextSupport::ApplicationContext]
    def build
      ApplicationContext.new(@object_repository)
    end

    # @param [Hash] options
    # @param [Proc] block
    # @return [LookmeContextSupport::ReloadableApplicationContext]
    def build_reloadable(options, &block)
      ReloadableApplicationContext.new(@object_repository, options, &block)
    end
  end
end