module LookmeContextSupport
  class ApplicationContext
    LOG_TAG = self.name

    attr_reader :object_repository # LookmeContextSupport::ObjectRepository

    # @param [LookmeContextSupport::ObjectRepository] object_repository
    def initialize(object_repository)
      @object_repository = object_repository
    end

    # @param [Symbol] name
    # @return [Object] object, maybe nil
    def get_object(name)
      @object_repository.get_object(name, false)
    end

    DEFAULT_PROFILE_NAME = :default

    @profiles = {}

    class << self
      def configure(options = {}, &block)
        profile_name = options[:profile_name] || DEFAULT_PROFILE_NAME
        builder = ApplicationContextBuilder.new
        builder.register(:profile_name, profile_name)
        block.call(builder)
        context = options[:reloadable] ? builder.build_reloadable(options, &block) : builder.build
        register_context(profile_name, context)
        context
      end

      def unregister_context(profile_name)
        logger.info(LOG_TAG) {"unregister context, profile #{profile_name}"}
        @profiles.delete(profile_name)
      end

      def register_context(profile_name, context)
        logger.info(LOG_TAG) {"register context, profile #{profile_name}"}
        @profiles[profile_name] = context
      end

      # @param [String] profile_name
      # @return [LookmeContextSupport::ApplicationContext]
      def get_context(profile_name)
        context = @profiles[profile_name]
        return context if context
        raise ProfileNotFoundError, "context not found, profile name #{profile_name}"
      end

      def get_default_context
        get_context(DEFAULT_PROFILE_NAME)
      end

      # @param [String] profile_name
      def reload_context!(profile_name)
        context = get_context(profile_name)
        if context.is_a?(ReloadableApplicationContext)
          logger.info(LOG_TAG) {"reload context, profile #{profile_name}"}
          context.reload!
        else
          raise UnexpectedApplicationContextError, "not a reloadable application context, #{context.class}"
        end
      end

      def reload_default_context!
        reload_context!(DEFAULT_PROFILE_NAME)
      end

      private

      def logger
        LookmeLogging::LoggerManager.find_logger(ApplicationContext)
      end
    end
  end
end