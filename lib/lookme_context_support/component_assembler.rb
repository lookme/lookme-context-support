module LookmeContextSupport
  class ComponentAssembler
    LOG_TAG = self.name
    NOT_ATTR_WRITE_METHODS = ::Set.new([:===, :==, :!=, :>=, :<=, :!=])

    attr_writer :dependency_required # Boolean

    # @param [LookmeContextSupport] object_repository
    # @param [Logger] logger
    # @param [Symbol] name
    # @param [Class] klass
    def initialize(object_repository, name, klass, logger)
      @logger = logger
      @object_repository = object_repository
      @name = name
      @klass = klass

      @dependency_mapping = {}
      @dependency_required = true
      @attribute_ignored = ::Set.new
    end

    def assemble
      object = @klass.new
      @logger.debug(LOG_TAG) {"assemble #{@name}, class #{@klass}"}
      filter_attribute_writers.each do |m|
        attribute_name = m.to_s[0, m.length - 1].to_sym
        populate_attribute(object, m, attribute_name)
      end
      object
    end

    # @param [Hash<Symbol,Symbol>] dependency_mapping
    def dependency_mapping=(dependency_mapping)
      return if dependency_mapping.nil?
      @dependency_mapping = dependency_mapping
    end

    # @param [Array<Symbol>] attribute_ignored
    def attribute_ignored=(attribute_ignored)
      return if attribute_ignored.nil?
      @attribute_ignored = ::Set.new(attribute_ignored)
    end

    private

    # @param [Object] object
    # @param [Symbol] method
    # @param [Symbol] attribute_name
    def populate_attribute(object, method, attribute_name)
      if @attribute_ignored.include?(attribute_name)
        @logger.debug(LOG_TAG) {"skip attribute #{attribute_name}"}
        return
      end

      dependency_name = @dependency_mapping[attribute_name] || attribute_name
      dependency_object = @object_repository.get_object(dependency_name, @dependency_required)
      if dependency_object.nil? && !@dependency_required
        @logger.debug(LOG_TAG) {"skip dependency #{dependency_name}"}
        return
      end

      @logger.debug(LOG_TAG) {"populate #{@name} with #{dependency_name}"}
      object.send(method, dependency_object)
    end

    # @return [Array<Symbol>]
    def filter_attribute_writers
      @klass.instance_methods.select {|m|
        !NOT_ATTR_WRITE_METHODS.include?(m) && m.to_s.end_with?('=')
      }
    end
  end
end