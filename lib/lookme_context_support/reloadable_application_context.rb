module LookmeContextSupport
  class ReloadableApplicationContext < ApplicationContext
    def initialize(object_repository, options, &block)
      super(object_repository)
      @options = options
      @block = block
    end

    def reload!
      ApplicationContext.configure(@options, &@block)
    end
  end
end