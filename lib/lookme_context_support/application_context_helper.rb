module LookmeContextSupport
  module ApplicationContextHelper
    def get_object_by_name(name)
      context = ApplicationContext.get_default_context
      object = context.get_object(name)
      return object if object
      raise ObjectNotFoundError, "object #{name} not found"
    end
  end
end