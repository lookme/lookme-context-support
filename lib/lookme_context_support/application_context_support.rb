module LookmeContextSupport
  module ApplicationContextSupport
    def self.included(base)
      base.extend(ClassMethods)
    end

    # Get object by name.
    # An ObjectNotFoundError will be raised if object not found.
    #
    # @param [String] name
    # @return [Object] object
    def get_object_by_name(name)
      profile_name = self.class.application_context_profile_name || ApplicationContext::DEFAULT_PROFILE_NAME
      context = ApplicationContext.get_context(profile_name)
      object = context.get_object(name)
      return object if object
      raise ObjectNotFoundError, "object #{name} not found"
    end

    module ClassMethods
      # @param [Symbol] name
      def use_application_context_profile_name(name)
        @application_context_profile_name = name
      end

      # @return [Symbol]
      def application_context_profile_name
        @application_context_profile_name
      end
    end
  end
end